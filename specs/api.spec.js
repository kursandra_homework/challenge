import { getChallenges} from "../framework/services/getChallenges.service";

describe ('description', () => {
    test('Получить список заданий', async() => {
      const {status} = await new getChallenges().get();
      expect(status).toBe(200);
    });
  }
)