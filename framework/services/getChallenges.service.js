import supertest from 'supertest';
import {urls} from "../config";

const getChallenges = function () {
  this.get = async function()
  {
    const r = await supertest(urls.apichallenges).get('/challenges');
    return r;
  }
}

export {getChallenges};